Attribute VB_Name = "Usefull_Macros"
Function SumByColor(CellColor As Range, rRange As Range)
' Sum by colour of cell
    Dim cSum As Long
    Dim ColIndex As Integer
    ColIndex = CellColor.Interior.ColorIndex
    For Each cl In rRange
        If cl.Interior.ColorIndex = ColIndex Then
            cSum = WorksheetFunction.Sum(cl, cSum)
        End If
    Next cl
    SumByColor = cSum
End Function

Function print_time(df)
    print_time = CStr(Int(df / 3600)) + "h " + CStr(Int((df / 60) Mod 60)) + "m " _
                    + CStr(Int(df Mod 60)) + "s"
End Function

Function print_time_in_seconds(iHour As Long, iMin As Integer, iSec As Integer) As Long
    Dim df As Long
    df = iHour * 3600
    print_time_in_seconds = df + iMin * 60 + iSec
End Function

Function GramPar(x0, y0, x1, y1, x)
    GramPar = y0 + (x - x0) * (y1 - y0) / (x1 - x0)
End Function

Sub DrawFilledPolygon()
' Draw a Polygon based on points of graph
    Dim myCht As Chart
    Dim mySrs As Series
    Dim Npts As Integer, Ipts As Integer
    Dim myBuilder As FreeformBuilder
    Dim myShape As Shape
    Dim Xnode As Double, Ynode As Double
    Dim Xmin As Double, Xmax As Double
    Dim Ymin As Double, Ymax As Double
    Dim Xleft As Double, Ytop As Double
    Dim Xwidth As Double, Yheight As Double
    
    Set myCht = ActiveChart
    Xleft = myCht.PlotArea.InsideLeft
    Xwidth = myCht.PlotArea.InsideWidth
    Ytop = myCht.PlotArea.InsideTop
    Yheight = myCht.PlotArea.InsideHeight
    Xmin = myCht.Axes(1).MinimumScale
    Xmax = myCht.Axes(1).MaximumScale
    Ymin = myCht.Axes(2).MinimumScale
    Ymax = myCht.Axes(2).MaximumScale
    
    Set mySrs = myCht.SeriesCollection(1)
    Npts = mySrs.points.count
    
    ' first point
    Xnode = Xleft + (mySrs.XValues(Npts) - Xmin) * Xwidth / (Xmax - Xmin)
    Ynode = Ytop + (Ymax - mySrs.Values(Npts)) * Yheight / (Ymax - Ymin)
    Set myBuilder = myCht.Shapes.BuildFreeform(msoEditingAuto, Xnode, Ynode)
    
    ' remaining points
    For Ipts = 1 To Npts
        Xnode = Xleft + (mySrs.XValues(Ipts) - Xmin) * Xwidth / (Xmax - Xmin)
        Ynode = Ytop + (Ymax - mySrs.Values(Ipts)) * Yheight / (Ymax - Ymin)
        myBuilder.AddNodes msoSegmentLine, msoEditingAuto, Xnode, Ynode
    Next
    
    Set myShape = myBuilder.ConvertToShape
    
    With myShape
        .ShapeStyle = msoShapeStylePreset9
        .Fill.Patterned msoPatternWideUpwardDiagonal
    End With
End Sub

Sub export_chart()
    Worksheets("cost_opt").ChartObjects("Chart 3").Chart.Export _
        Filename:="current_chart.png", FilterName:="PNG", Interactive:=True
End Sub
